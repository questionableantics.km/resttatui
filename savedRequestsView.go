package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

// //////////////////////////////////////////////////////////////////////////
// Structs
// //////////////////////////////////////////////////////////////////////////

type SavedRequest struct {
	Id      int    `db:"id"`
	Name    string `db:"name"`
	Url     string `db:"url"`
	Method  string `db:"method"`
	Body    string `db:"body"`
	Headers string `db:"headers"`
	Params  string `db:"params"`
}

func (s SavedRequest) Title() string       { return s.Name }
func (s SavedRequest) Description() string { return s.Url }
func (s SavedRequest) FilterValue() string { return s.Name }

// //////////////////////////////////////////////////////////////////////////
// Helper functions
// //////////////////////////////////////////////////////////////////////////

func (m State) GetSavedRequests() []SavedRequest {
	savedRequests := []SavedRequest{}

	// Read the curl files from the directory using os package
	files, err := os.ReadDir("./curl_files")
	if err != nil {
		log.Fatal(err)
	}

	// Loop over the files
	for _, file := range files {
		// Open the file using os package
		f, err := os.Open("./curl_files/" + file.Name())
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()

		// Decode the file into a SavedRequest
		var sr SavedRequest
		decoder := json.NewDecoder(f)
		err = decoder.Decode(&sr)
		if err != nil {
			log.Fatal(err)
		}

		// Add the SavedRequest to the slice
		savedRequests = append(savedRequests, sr)
	}

	return savedRequests
}

func (m State) DeleteSavedRequest() ([]SavedRequest, error) {
	currentItem := m.savedRequestsView.SelectedItem()

	if currentItem == nil {
		return m.savedRequests, nil
	}

	// Delete the file
	err := os.Remove("./curl_files/" + currentItem.(SavedRequest).Name)
	if err != nil {
		fmt.Println(err)

		return m.savedRequests, err
	}

	return m.GetSavedRequests(), nil

}

func buildSavedRequestListItems(savedRequests []SavedRequest) []list.Item {
	items := []list.Item{}
	for _, savedRequest := range savedRequests {
		items = append(items, savedRequest)
	}

	return items
}

func NewSavedRequestsView(viewSize ViewSize, state State) list.Model {
	savedRequestsView := list.New(
		buildSavedRequestListItems(state.GetSavedRequests()),
		list.NewDefaultDelegate(),
		viewSize.RightPaneWidth,
		viewSize.TabContentHeight,
	)
	savedRequestsView.Title = "Saved Requests"

	return savedRequestsView
}

// //////////////////////////////////////////////////////////////////////////
// Bubble Tea
// //////////////////////////////////////////////////////////////////////////

func (m State) HandleSavedRequestsViewUpdate(msg tea.Msg) (State, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		key := msg.String()

		switch key {
		case "enter":
			request := m.savedRequestsView.SelectedItem().(SavedRequest)

			m = m.SetRequest(request)
			m.db.NamedExec(`
			UPDATE current_choices
			SET saved_request_id = :saved_request_id
			`, map[string]interface{}{
				"saved_request_id": request.Id,
			})

			m, m.currentBuffer, cmd = GoTo(m.currentBuffer, UrlNode, m)

			return m, cmd
		case "j", "down", "ctrl+n":
			m.savedRequestsView.CursorDown()

			return m, nil
		case "k", "up", "ctrl+p":
			m.savedRequestsView.CursorUp()

			return m, nil
		case "l":
			m.savedRequestsView.Paginator.NextPage()

			return m, nil
		case "h":
			m.savedRequestsView.Paginator.PrevPage()

			return m, nil
		case "d":
			savedRequests, _ := m.DeleteSavedRequest()

			cmd = m.savedRequestsView.SetItems(buildSavedRequestListItems(savedRequests))

			return m, cmd
		case "n":
			m = m.SetRequest(SavedRequest{Method: "GET"})
			m, m.currentBuffer, cmd = GoTo(m.currentBuffer, UrlNode, m)

			return m, cmd
		}
	}

	return m, cmd
}

func (m State) RenderSavedRequests() string {
	m.savedRequestsView.SetHeight(m.viewSize.TabContentHeight)
	m.savedRequestsView.SetWidth(m.viewSize.RightPaneWidth)

	style := lipgloss.
		NewStyle().
		Border(lipgloss.RoundedBorder()).
		Height(m.viewSize.TabContentHeight).
		Width(m.viewSize.LeftPaneWidth)

	if m.currentBuffer == LeftPaneTabsNode && m.currentLeftPaneTab == SavedRequestsNode {
		style = style.BorderForeground(lipgloss.Color("#00ff9c"))
	}

	return style.Render(m.savedRequestsView.View())
}
