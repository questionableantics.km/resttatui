package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

// //////////////////////////////////////////////////////////////////////////
// Variables
// //////////////////////////////////////////////////////////////////////////

var (
	GetNode         = &StringNode{Name: "GET"}
	PostNode        = &StringNode{Name: "POST"}
	PatchNode       = &StringNode{Name: "PATCH"}
	DeleteNode      = &StringNode{Name: "DELETE"}
	PutNode         = &StringNode{Name: "PUT"}
	focusedUrlInput = "URL"
)

// //////////////////////////////////////////////////////////////////////////
// Helper functions
// //////////////////////////////////////////////////////////////////////////

func initializeMethodOrder() {
	GetNode.Next = PostNode
	PostNode.Next = PatchNode
	PatchNode.Next = DeleteNode
	DeleteNode.Next = PutNode
	PutNode.Next = GetNode

	GetNode.Prev = PutNode
	PutNode.Prev = DeleteNode
	DeleteNode.Prev = PatchNode
	PatchNode.Prev = PostNode
	PostNode.Prev = GetNode
}

func (m State) UpdateWithEnvironment(str string) string {
	for key, value := range m.environment.current.variables {
		str = strings.ReplaceAll(str, fmt.Sprintf("{{%s}}", key), value)
	}

	return str
}

func (m State) SetRequest(request SavedRequest) State {
	switch request.Method {
	case "GET":
		m.currentMethod = GetNode
	case "POST":
		m.currentMethod = PostNode
	case "PATCH":
		m.currentMethod = PatchNode
	case "DELETE":
		m.currentMethod = DeleteNode
	case "PUT":
		m.currentMethod = PutNode
	default:
		m.currentMethod = GetNode
	}
	m.urlInputView.SetValue(request.Url)
	m.bodyView.SetValue(request.Body)
	m.headersView.SetValue(request.Headers)
	m.paramsView.SetValue(request.Params)
	m.responseView.SetContent("")
	m.nameInputView.SetValue(request.Name)
	m.currentRequestId = request.Id

	return m
}

func parseHeaders(headers string) map[string]string {
	headersMap := make(map[string]string)

	for _, header := range strings.Split(headers, "\n") {
		if header == "" {
			continue
		}

		headerParts := strings.Split(header, ":")
		headersMap[headerParts[0]] = headerParts[1]
	}

	return headersMap
}

func parseQueryParams(params string) map[string]string {
	paramsMap := make(map[string]string)

	for _, param := range strings.Split(params, "\n") {
		if param == "" {
			continue
		}

		paramParts := strings.Split(param, ":")
		paramsMap[paramParts[0]] = paramParts[1]
	}

	return paramsMap
}

func formatQueryParams(params map[string]string) string {
	queryParams := []string{}

	for key, value := range params {
		queryParams = append(queryParams, fmt.Sprintf("%s=%s", key, value))
	}

	return strings.Join(queryParams, "&")
}

func (m State) MakeRequest() ([]byte, error) {
	method := m.currentMethod.Name
	url := m.UpdateWithEnvironment(m.urlInputView.Value())
	headers := parseHeaders(m.UpdateWithEnvironment(m.headersView.Value()))
	params := parseQueryParams(m.UpdateWithEnvironment(m.paramsView.Value()))
	body := m.UpdateWithEnvironment(m.bodyView.Value())

	url = fmt.Sprintf("%s?%s", url, formatQueryParams(params))

	request, err := http.NewRequest(method, url, nil)

	for key, value := range headers {
		request.Header.Add(key, value)
	}

	switch method {
	case "POST", "PUT", "PATCH":
		requestBody := []byte{}

		if body != "" {
			requestBody, err = json.Marshal(body)
		}

		if err != nil {
			return nil, err
		}

		request.Body = io.NopCloser(bytes.NewBuffer(requestBody))
		request.Header.Set("Content-Type", "application/json")
	}

	resp, err := http.DefaultClient.Do(request)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	respString, _ := io.ReadAll(resp.Body)

	return respString, nil
}

func NewUrlInputView(viewSize ViewSize) textinput.Model {
	urlInput := textinput.New()
	urlInput.Placeholder = "URL"
	urlInput.Focus()
	urlInput.SetValue(DefaultUrl)

	return urlInput
}

func NewNameInputView() textinput.Model {
	nameInput := textinput.New()
	nameInput.Placeholder = "Name"

	return nameInput
}

// //////////////////////////////////////////////////////////////////////////
// Bubble Tea
// //////////////////////////////////////////////////////////////////////////

func (m State) HandleUrlViewUpdate(msg tea.Msg) (State, tea.Cmd) {
	var cmd tea.Cmd
	var cmds []tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		key := msg.String()
		switch key {
		case "enter":
			resp, err := m.MakeRequest()

			if err != nil {
				m.responseView.SetContent(fmt.Sprintf("%s", err))
				m.urlInputView.Blur()

				return m, nil
			}

			m.urlInputView.Blur()
			m, m.currentBuffer, cmd = GoTo(m.currentBuffer, RightPaneTabsNode, m)
			cmds = append(cmds, cmd)
			m, m.currentRightPaneTab, cmd = GoTo(m.currentRightPaneTab, ResponseNode, m)
			cmds = append(cmds, cmd)
			m.response = formatJson(resp)
			m.responseView.SetContent(m.response)
			m.responseView.GotoTop()

			return m, tea.Batch(cmds...)
		case "ctrl+l", "ctrl+h":
			switch focusedUrlInput {
			case "URL":
				focusedUrlInput = "METHOD"
			default:
				focusedUrlInput = "URL"
			}

			return m, nil
		}

		switch focusedUrlInput {
		case "URL":
			m.urlInputView, cmd = m.urlInputView.Update(msg)

			return m, cmd
		case "METHOD":
			switch key {
			case "ctrl+n":
				m.currentMethod = m.currentMethod.Next

				return m, nil
			case "ctrl+p":
				m.currentMethod = m.currentMethod.Prev

				return m, nil
			}
		}
	}

	return m, cmd
}

func (m State) RenderUrlInput() string {
	methodStyle := lipgloss.NewStyle().
		Border(lipgloss.RoundedBorder()).
		Height(m.viewSize.UrlHeight).
		Width(6)

	urlStyle := lipgloss.NewStyle().
		Border(lipgloss.RoundedBorder()).
		Height(m.viewSize.UrlHeight).
		Width(m.viewSize.RightPaneWidth - methodStyle.GetWidth() - 2)

	if m.savingWorkflow.IsSaving {
		m.urlInputView.Blur()
		m.nameInputView.Focus()

		if m.currentBuffer == UrlNode {
			return urlStyle.
				BorderForeground(lipgloss.Color("#00ff9c")).
				Render(m.nameInputView.View())
		} else {
			return urlStyle.Render(m.nameInputView.View())
		}
	}

	if m.currentBuffer == UrlNode {
		if focusedUrlInput == "URL" {
			urlStyle = urlStyle.BorderForeground(lipgloss.Color("#00ff9c"))
		} else {
			methodStyle = methodStyle.BorderForeground(lipgloss.Color("#00ff9c"))
		}
	}

	return lipgloss.JoinHorizontal(lipgloss.Left, urlStyle.Render(m.urlInputView.View()), methodStyle.Render(m.currentMethod.Name))
}
