package main

import (
	"github.com/charmbracelet/bubbles/textarea"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

func (m State) HandleParamsViewUpdate(msg tea.Msg) (State, tea.Cmd) {
	var cmd tea.Cmd

	m.paramsView, cmd = m.paramsView.Update(msg)

	return m, cmd
}

func NewParamsView(viewSize ViewSize) textarea.Model {
	paramsView := textarea.New()
	paramsView.SetHeight(viewSize.TabContentHeight + 2)
	paramsView.SetWidth(viewSize.RightPaneWidth + 24)

	return paramsView
}

// //////////////////////////////////////////////////////////////////////////
// Bubble Tea
// //////////////////////////////////////////////////////////////////////////

func (m State) RenderParams() string {
	m.paramsView.SetWidth(m.viewSize.RightPaneWidth)
	m.paramsView.SetHeight(m.viewSize.TabContentHeight)
	paramsStyle := lipgloss.NewStyle().
		Border(lipgloss.RoundedBorder()).
		Width(m.viewSize.RightPaneWidth).
		Height(m.viewSize.TabContentHeight)

	if m.currentRightPaneTab == ParamsNode && m.currentBuffer.Name == RightPaneTabsNode.Name {
		return paramsStyle.
			BorderForeground(lipgloss.Color("#00ff9c")).
			Render(m.paramsView.View())
	}

	return paramsStyle.Render(m.paramsView.View())
}
