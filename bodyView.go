package main

import (
	"github.com/charmbracelet/bubbles/textarea"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

func (m State) HandleBodyViewUpdate(msg tea.Msg) (State, tea.Cmd) {
	var cmd tea.Cmd

	m.bodyView, cmd = m.bodyView.Update(msg)

	return m, cmd
}

func NewBodyView(viewSize ViewSize) textarea.Model {
	bodyView := textarea.New()
	bodyView.SetHeight(viewSize.TabContentHeight)
	bodyView.SetWidth(viewSize.RightPaneWidth)
	return bodyView
}

// //////////////////////////////////////////////////////////////////////////
// Bubble Tea
// //////////////////////////////////////////////////////////////////////////

func (m State) RenderBody() string {
	m.bodyView.SetWidth(m.viewSize.RightPaneWidth)
	m.bodyView.SetHeight(m.viewSize.TabContentHeight)

	bodyStyle := lipgloss.NewStyle().
		Border(lipgloss.RoundedBorder()).
		Width(m.viewSize.RightPaneWidth).
		Height(m.viewSize.TabContentHeight)

	if m.currentRightPaneTab == BodyNode && m.currentBuffer.Name == RightPaneTabsNode.Name {
		return bodyStyle.
			BorderForeground(lipgloss.Color("#00ff9c")).
			Render(m.bodyView.View())
	}

	return bodyStyle.Render(colorizeJson(m.bodyView.Value()))
}
