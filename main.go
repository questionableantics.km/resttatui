package main

import (
	"fmt"
	"os"

	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/textarea"
	"github.com/charmbracelet/bubbles/textinput"
	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

// //////////////////////////////////////////////////////////////////////////
// Structs
// //////////////////////////////////////////////////////////////////////////

type ViewSize struct {
	Height            int
	Width             int
	UrlHeight         int
	TabHeaderHeight   int
	TabContentHeight  int
	EnvironmentHeight int
	LeftPaneWidth     int
	RightPaneWidth    int
}

type SavingWorkflow struct {
	IsSaving bool
}

func (v *ViewSize) UpdateSize(width int, height int) {
	v.Width = width - 6
	v.Height = height - 6
	v.UrlHeight = 1
	v.TabHeaderHeight = 1
	v.TabContentHeight = v.Height - v.UrlHeight - v.TabHeaderHeight - 2
	v.EnvironmentHeight = 1
	v.LeftPaneWidth = v.Width * 3 / 10
	v.RightPaneWidth = v.Width - v.LeftPaneWidth
}

type State struct {
	bodyView            textarea.Model
	currentBuffer       *StringNode
	currentMethod       *StringNode
	currentLeftPaneTab  *StringNode
	currentRequestId    int
	currentRightPaneTab *StringNode
	db                  *sqlx.DB
	environment         EnvironmentState
	headersView         textarea.Model
	nameInputView       textinput.Model
	paramsView          textarea.Model
	responseView        viewport.Model
	response            string
	savedRequestsView   list.Model
	savedRequests       []SavedRequest
	savingWorkflow      SavingWorkflow
	urlInputView        textinput.Model
	viewSize            ViewSize
}

// //////////////////////////////////////////////////////////////////////////
// Consts
// //////////////////////////////////////////////////////////////////////////

const DefaultUrl = "https://pokeapi.co/api/v2/pokemon"

type StringNode struct {
	Name    string
	Next    *StringNode
	Prev    *StringNode
	OnBlur  func(State) (State, tea.Cmd)
	OnFocus func(State) (State, tea.Cmd)
}

var (
	UrlNode = &StringNode{
		Name: "URL",
		OnBlur: func(m State) (State, tea.Cmd) {
			m.urlInputView.Blur()

			return m, nil
		},
		OnFocus: func(m State) (State, tea.Cmd) {
			m.urlInputView.Focus()

			return m, nil
		},
	}
	RightPaneTabsNode = &StringNode{
		Name: "Right Pane Tabs",
	}
	SavedRequestsNode = &StringNode{
		Name: "Saved Requests",
	}
	LeftPaneTabsNode = &StringNode{
		Name: "Left Pane Tabs",
	}
	EnvironmentNode = &StringNode{
		Name: "Environment",
		Next: SavedRequestsNode,
		Prev: RightPaneTabsNode,
	}
)

// //////////////////////////////////////////////////////////////////////////
// Helper functions
// //////////////////////////////////////////////////////////////////////////

func (m State) SaveRequest() ([]SavedRequest, error) {
	var err error
	url := m.urlInputView.Value()
	method := m.currentMethod.Name
	body := m.bodyView.Value()
	headers := m.headersView.Value()
	params := m.paramsView.Value()
	name := m.nameInputView.Value()
	currentRequestId := m.currentRequestId

	if currentRequestId == 0 {
		_, err = m.db.NamedExec(`
			INSERT INTO saved_requests (name, url, method, body, headers, params)
			VALUES (:name, :url, :method, :body, :headers, :params)
			`,
			map[string]interface{}{
				"name":    name,
				"url":     url,
				"method":  method,
				"body":    body,
				"headers": headers,
				"params":  params,
			})
	} else {
		_, err = m.db.NamedExec(
			`
			UPDATE saved_requests
			SET name = :name,
				url = :url,
				method = :method,
				body = :body,
				headers = :headers,
				params = :params
			WHERE id = :id
			`,
			map[string]interface{}{
				"name":    name,
				"url":     url,
				"method":  method,
				"body":    body,
				"headers": headers,
				"params":  params,
				"id":      currentRequestId,
			})
	}

	if err != nil {
		fmt.Println(err)

		return m.savedRequests, err
	}

	return m.GetSavedRequests(), nil
}

func (m State) SaveCurrentRequest() (State, error) {
	savedRequests, err := m.SaveRequest()

	if err != nil {
		fmt.Println(err)

		return m, err
	}

	m.savedRequestsView = list.New(
		buildSavedRequestListItems(savedRequests),
		list.NewDefaultDelegate(),
		m.viewSize.RightPaneWidth,
		m.viewSize.TabContentHeight,
	)

	m.savingWorkflow.IsSaving = false
	m.nameInputView.Blur()
	m, m.currentBuffer, _ = GoTo(m.currentBuffer, UrlNode, m)

	return m, nil
}

func initializeBufferOrder() {
	UrlNode.Next = RightPaneTabsNode
	UrlNode.Prev = LeftPaneTabsNode
	RightPaneTabsNode.Next = LeftPaneTabsNode
	RightPaneTabsNode.Prev = UrlNode
	LeftPaneTabsNode.Next = UrlNode
	LeftPaneTabsNode.Prev = RightPaneTabsNode
}

// //////////////////////////////////////////////////////////////////////////
// Bubble Tea
// //////////////////////////////////////////////////////////////////////////

func initialModel() State {
	db, err := sqlx.Open("sqlite3", "./resttatui.db")

	if err != nil {
		fmt.Println(err)
	}

	err = CreateDatabase(db)

	if err != nil {
		fmt.Println(err)

		os.Exit(1)
	}

	viewSize := ViewSize{}
	viewSize.UpdateSize(100, 20)

	state := State{
		currentBuffer:       UrlNode,
		currentMethod:       GetNode,
		currentRightPaneTab: ResponseNode,
		currentLeftPaneTab:  SavedRequestsNode,
		db:                  db,
		savingWorkflow: SavingWorkflow{
			IsSaving: false,
		},
		viewSize: viewSize,
	}

	initializeBufferOrder()
	initializeTabOrder()
	initializeMethodOrder()

	state.responseView = NewResponseView(viewSize)
	state.nameInputView = NewNameInputView()
	state.urlInputView = NewUrlInputView(viewSize)
	state.bodyView = NewBodyView(viewSize)
	state.headersView = NewHeadersView(viewSize)
	state.paramsView = NewParamsView(viewSize)
	state.environment = NewEnvironmentView(viewSize, state)
	state.savedRequestsView = NewSavedRequestsView(viewSize, state)

	currentRequest := &SavedRequest{}
	err = state.db.Get(
		currentRequest,
		`
		select *
		from saved_requests
		where id = (
			select saved_request_id
			from current_choices
		);
		`,
	)
	state = state.SetRequest(*currentRequest)

	return state
}

func (m State) Init() tea.Cmd {
	return nil
}

func (m State) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	var cmds []tea.Cmd

	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.viewSize.UpdateSize(msg.Width, msg.Height)

		return m, nil
	case tea.KeyMsg:
		key := msg.String()

		if m.savingWorkflow.IsSaving {
			m.urlInputView.Blur()
			m.nameInputView.Focus()
			m.nameInputView, cmd = m.nameInputView.Update(msg)

			switch key {
			case "enter":
				m, _ := m.SaveCurrentRequest()

				return m, cmd
			case "ctrl+c", "esc":
				m.savingWorkflow.IsSaving = false
				m.nameInputView.Blur()
				m.urlInputView.Focus()

				return m, nil
			}
		}

		switch key {
		case "ctrl+c", "esc":
			return m, tea.Quit
		case "tab":
			m, cmd = m.GoNextBuffer()

			return m, cmd
		case "shift+tab":
			m, cmd = m.GoPrevBuffer()

			return m, cmd
		case "ctrl+s":
			m, m.currentBuffer, cmd = GoTo(m.currentBuffer, UrlNode, m)
			m.savingWorkflow.IsSaving = true

			return m, nil
		case "ctrl+e":
			m, m.currentBuffer, cmd = GoTo(m.currentBuffer, LeftPaneTabsNode, m)
			cmds = append(cmds, cmd)
			m, m.currentLeftPaneTab, cmd = GoTo(m.currentLeftPaneTab, EnvironmentNode, m)
			cmds = append(cmds, cmd)

			return m, tea.Batch(cmds...)
		}

		switch m.currentBuffer {
		case UrlNode:
			return m.HandleUrlViewUpdate(msg)
		case RightPaneTabsNode:
			switch key {
			case "ctrl+l":
				return m.GoNextRightPaneTab()
			case "ctrl+h":
				return m.GoPrevRightPaneTab()
			}

			switch m.currentRightPaneTab {
			case BodyNode:
				return m.HandleBodyViewUpdate(msg)
			case HeadersNode:
				return m.HandleHeadersViewUpdate(msg)
			case ParamsNode:
				return m.HandleParamsViewUpdate(msg)
			case ResponseNode:
				return m.HandleResponseViewUpdate(msg)
			}
		case LeftPaneTabsNode:
			switch key {
			case "ctrl+l", "ctrl+h":
				if m.currentLeftPaneTab == EnvironmentNode {
					m, cmd = m.GoToLeftPaneTab(SavedRequestsNode)
				} else {
					m, cmd = m.GoToLeftPaneTab(EnvironmentNode)
				}

				return m, cmd
			}

			switch m.currentLeftPaneTab {
			case EnvironmentNode, EnvironmentEditNode, EnvironmentNameEditNode:
				return m.HandleEnvironmentViewUpdate(msg)
			case SavedRequestsNode:
				return m.HandleSavedRequestsViewUpdate(msg)
			}
		}

		switch key {
		case "q":
			return m, tea.Quit
		case "ctrl+l":
			m, m.currentBuffer, cmd = GoTo(m.currentBuffer, UrlNode, m)

			return m, cmd
		case "ctrl+r":
			m, m.currentBuffer, cmd = GoTo(m.currentBuffer, SavedRequestsNode, m)

			return m, cmd
		case "ctrl+b":
			m, m.currentBuffer, cmd = GoTo(m.currentBuffer, RightPaneTabsNode, m)
			cmds = append(cmds, cmd)
			m, m.currentRightPaneTab, cmd = GoTo(m.currentRightPaneTab, BodyNode, m)
			cmds = append(cmds, cmd)

			return m, tea.Batch(cmds...)
		case "ctrl+x":
			return m.SetRequest(SavedRequest{Method: "GET"}), nil
		}

	}

	return m, cmd
}

func renderCurrentTab(m State) string {
	switch m.currentRightPaneTab {
	case ResponseNode:
		return m.RenderResponse()
	case BodyNode:
		return m.RenderBody()
	case HeadersNode:
		return m.RenderHeaders()
	case ParamsNode:
		return m.RenderParams()
	default:
		return m.RenderResponse()
	}
}

var LeftPaneTabs = []string{
	SavedRequestsNode.Name,
	EnvironmentNode.Name,
}

func (m State) RenderLeftPaneTabs() string {
	var tabs []string

	style := lipgloss.NewStyle().
		BorderBottom(true).
		Width(m.viewSize.LeftPaneWidth / len(LeftPaneTabs)).
		Height(m.viewSize.TabHeaderHeight).
		Align(lipgloss.Center)

	focusedStyle := style.Copy().
		Foreground(lipgloss.Color("#00ff9c")).
		BorderBottom(true)

	for _, tab := range LeftPaneTabs {
		if tab == m.currentLeftPaneTab.Name {
			tabs = append(tabs, focusedStyle.Render(tab))
		} else {
			tabs = append(tabs, style.Render(tab))
		}
	}

	return lipgloss.NewStyle().
		BorderLeft(true).
		BorderRight(true).
		BorderForeground(lipgloss.Color("#00ff9c")).
		Render(lipgloss.JoinHorizontal(lipgloss.Center, tabs...))
}

func renderLeftPane(m State) string {
	top := lipgloss.JoinVertical(
		lipgloss.Left,
		m.RenderEnvironmentName(),
		m.RenderLeftPaneTabs(),
	)

	switch m.currentLeftPaneTab {
	case EnvironmentNode, EnvironmentEditNode, EnvironmentNameEditNode:
		return lipgloss.JoinVertical(
			lipgloss.Left,
			top,
			m.RenderEnvironment(),
		)
	default:
		return lipgloss.JoinVertical(
			lipgloss.Left,
			top,
			m.RenderSavedRequests(),
		)
	}
}

func (m State) View() string {
	urlView := m.RenderUrlInput()
	leftPane := renderLeftPane(m)
	tabs := m.RenderTabs()
	currentTab := renderCurrentTab(m)

	var view string

	view = lipgloss.JoinVertical(
		lipgloss.Center,
		urlView,
		tabs,
		currentTab,
	)

	view = lipgloss.JoinHorizontal(lipgloss.Left, leftPane, view)

	return view
}

func main() {
	p := tea.NewProgram(initialModel())

	if _, err := p.Run(); err != nil {
		fmt.Println("Error running program:", err)
		os.Exit(1)
	}
}
