package main

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/textarea"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type Environment struct {
	Id        int    `db:"id"`
	Name      string `db:"name"`
	Variables string `db:"variables"`
}

type CurrentEnvironment struct {
	variables map[string]string
	name      string
}

func (e Environment) ParseVariables() map[string]string {
	variables := make(map[string]string)

	for _, line := range strings.Split(e.Variables, "\n") {
		if line == "" {
			continue
		}

		rgx := regexp.MustCompile(`:\s*`)
		parts := rgx.Split(line, 2)

		if len(parts) != 2 {
			continue
		}

		key := parts[0]
		value := parts[1]

		variables[key] = value
	}

	return variables
}

func (e Environment) Title() string       { return e.Name }
func (e Environment) Description() string { return "" }
func (e Environment) FilterValue() string { return e.Name }

func (e *Environment) Save(m State) error {
	var err error

	if e.Name == "" {
		err = error(fmt.Errorf("Environment name cannot be empty"))
	} else if e.Id == 0 {
		res, _ := m.db.NamedExec(`
			INSERT INTO environments (name, variables)
			VALUES (:name, :variables);
			`,
			map[string]interface{}{
				"name":      e.Name,
				"variables": m.environment.editView.Value(),
			},
		)

		lastRowId, _ := res.LastInsertId()

		e.Id = int(lastRowId)
	} else {
		_, err = m.db.NamedExec(`
			UPDATE environments
			SET name = :name,
				variables = :variables
			WHERE id = :id;
			`,
			map[string]interface{}{
				"id":        e.Id,
				"name":      e.Name,
				"variables": m.environment.editView.Value(),
			},
		)
	}

	if err != nil {
		fmt.Println("Error saving environment")
		fmt.Println(err)
	}

	return err
}

func (e Environment) Delete(m State) error {
	var err error

	if e.Id == 0 {
		return nil
	}

	m.db.NamedExec(`
		DELETE FROM environments
		WHERE id = :id;
		`,
		map[string]interface{}{
			"id": e.Id,
		},
	)

	return err
}

type EnvironmentState struct {
	current      CurrentEnvironment
	listView     list.Model
	editView     textarea.Model
	editNameView textinput.Model
}

var (
	EnvironmentEditNode = &StringNode{
		Name: "Edit Environment",
		OnBlur: func(m State) (State, tea.Cmd) {
			selectedEnvironment := m.environment.listView.SelectedItem().(Environment)
			index := m.environment.listView.Index()

			selectedEnvironment.Variables = m.environment.editView.Value()
			selectedEnvironment.Save(m)
			cmd := m.environment.listView.SetItem(index, selectedEnvironment)
			m.environment.editView.Blur()

			return m, cmd
		},
		OnFocus: func(m State) (State, tea.Cmd) {
			m.environment.editView.Focus()

			return m, nil
		},
	}
	EnvironmentNameEditNode = &StringNode{
		Name: "Edit Environment Name",
		OnBlur: func(m State) (State, tea.Cmd) {
			selectedEnvironment := m.environment.listView.SelectedItem().(Environment)
			index := m.environment.listView.Index()

			selectedEnvironment.Name = m.environment.editNameView.Value()
			selectedEnvironment.Save(m)
			cmd := m.environment.listView.SetItem(index, selectedEnvironment)
			m.environment.editNameView.Blur()

			return m, cmd
		},
		OnFocus: func(m State) (State, tea.Cmd) {
			cmd := m.environment.editNameView.Focus()

			return m, cmd
		},
	}
)

func NewEnvironmentView(viewSize ViewSize, state State) EnvironmentState {
	EnvironmentEditNode.Next = EnvironmentNode
	EnvironmentEditNode.Prev = EnvironmentNode
	EnvironmentNameEditNode.Next = EnvironmentNode
	EnvironmentNameEditNode.Prev = EnvironmentNode

	environmentState := EnvironmentState{}

	environments := []Environment{}
	err := state.db.Select(&environments, "select * from environments;")

	if err != nil {
		panic(err)
	}

	currentEnvironment := Environment{}
	err = state.db.Get(
		&currentEnvironment,
		`
		select *
		from environments where id = (
			select environment_id
			from current_choices
		);
		`,
	)

	if err == nil {
		environmentState.current = CurrentEnvironment{
			variables: currentEnvironment.ParseVariables(),
			name:      currentEnvironment.Name,
		}
	}

	items := []list.Item{}

	for _, env := range environments {
		items = append(items, env)
	}

	// Edit
	environmentState.editView = textarea.New()
	environmentState.editView.SetWidth(viewSize.LeftPaneWidth)
	environmentState.editView.SetHeight(viewSize.TabContentHeight)

	// List
	environmentState.listView = list.New(
		items,
		list.NewDefaultDelegate(),
		viewSize.LeftPaneWidth,
		viewSize.TabContentHeight,
	)
	environmentState.listView.Title = "Environments"

	// Name
	environmentState.editNameView = textinput.New()
	environmentState.editNameView.Placeholder = "Environment Name"
	environmentState.editNameView.Width = viewSize.LeftPaneWidth
	environmentState.editNameView.SetValue(environmentState.current.name)
	environmentState.editNameView.Prompt = ""

	return environmentState
}

func (m State) HandleEnvironmentViewUpdate(msg tea.Msg) (State, tea.Cmd) {
	var cmd tea.Cmd
	var cmds []tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		key := msg.String()

		switch m.currentLeftPaneTab {
		case EnvironmentEditNode:
			switch key {
			case "ctrl+l", "ctrl+h":
				m, m.currentLeftPaneTab, cmd = GoTo(m.currentLeftPaneTab, EnvironmentNameEditNode, m)

				return m, cmd
			case "ctrl+s":
				selectedEnvironment := m.environment.listView.SelectedItem().(Environment)

				selectedEnvironment.Save(m)

				m, m.currentLeftPaneTab, cmd = GoTo(m.currentLeftPaneTab, EnvironmentNode, m)

				return m, cmd
			}

			m.environment.editView, cmd = m.environment.editView.Update(msg)

			return m, cmd
		case EnvironmentNameEditNode:
			switch key {
			case "ctrl+l", "ctrl+h":
				m, m.currentLeftPaneTab, cmd = GoTo(m.currentLeftPaneTab, EnvironmentEditNode, m)

				return m, cmd
			case "ctrl+s", "enter":
				m, m.currentLeftPaneTab, cmd = GoTo(m.currentLeftPaneTab, EnvironmentEditNode, m)

				return m, cmd
			}

			m.environment.editNameView, cmd = m.environment.editNameView.Update(msg)

			return m, cmd
		case EnvironmentNode:
			switch key {
			case "enter":
				selectedEnvironment := m.environment.listView.SelectedItem().(Environment)
				m.environment.current = CurrentEnvironment{
					variables: selectedEnvironment.ParseVariables(),
					name:      selectedEnvironment.Name,
				}
				m.environment.editNameView.SetValue(selectedEnvironment.Name)
				m, m.currentBuffer, cmd = GoTo(m.currentBuffer, LeftPaneTabsNode, m)

				m.db.NamedExec(`
				UPDATE current_choices
				SET environment_id = :environment_id
			`, map[string]interface{}{
					"environment_id": selectedEnvironment.Id,
				})

				return m, cmd
			case "j", "down", "ctrl+n":
				m.environment.listView.CursorDown()

				return m, nil
			case "k", "up", "ctrl+p":
				m.environment.listView.CursorUp()

				return m, nil
			case "l":
				m.environment.listView.Paginator.NextPage()

				return m, nil
			case "h":
				m.environment.listView.Paginator.PrevPage()

				return m, nil
			case "d":
				selectedEnvironment := m.environment.listView.SelectedItem().(Environment)

				m.environment.listView.RemoveItem(m.environment.listView.Index())
				selectedEnvironment.Delete(m)

				return m, nil
			case "n":
				m.environment.editNameView.SetValue("")
				m.environment.editView.SetValue("")
				m.environment.listView.InsertItem(0, Environment{})
				m, m.currentLeftPaneTab, cmd = GoTo(m.currentLeftPaneTab, EnvironmentNameEditNode, m)

				return m, cmd
			case "e":
				items := m.environment.listView.Items()

				if len(items) == 0 {
					return m, nil
				}

				env := m.environment.listView.SelectedItem().(Environment)

				m.environment.editView.SetValue(env.Variables)

				m, m.currentBuffer, cmd = GoTo(m.currentBuffer, LeftPaneTabsNode, m)
				cmds = append(cmds, cmd)
				m, m.currentLeftPaneTab, cmd = GoTo(m.currentLeftPaneTab, EnvironmentEditNode, m)
				cmds = append(cmds, cmd)

				return m, tea.Batch(cmds...)
			}
		}
	}

	return m, cmd
}

func (m State) RenderEnvironmentName() string {
	style := lipgloss.NewStyle().
		Border(lipgloss.RoundedBorder()).
		Width(m.viewSize.LeftPaneWidth).
		Height(1).
		Align(lipgloss.Center)

	if m.currentBuffer == LeftPaneTabsNode && m.currentLeftPaneTab != SavedRequestsNode {
		style = style.BorderForeground(lipgloss.Color("#00ff9c"))
	}

	return style.Render(lipgloss.JoinHorizontal(lipgloss.Center, "Environment: ", m.environment.editNameView.View()))
}

func (m State) RenderEnvironmentEdit() string {
	m.environment.editView.SetWidth(m.viewSize.LeftPaneWidth)
	m.environment.editView.SetHeight(m.viewSize.TabContentHeight)

	style := lipgloss.NewStyle().
		Border(lipgloss.RoundedBorder()).
		Width(m.viewSize.LeftPaneWidth).
		Height(m.viewSize.TabContentHeight)

	if m.currentBuffer == LeftPaneTabsNode {
		style = style.BorderForeground(lipgloss.Color("#00ff9c"))
	}

	return style.Render(m.environment.editView.View())
}

func (m State) RenderEnvironmentList() string {
	m.environment.listView.SetWidth(m.viewSize.LeftPaneWidth)
	m.environment.listView.SetHeight(m.viewSize.TabContentHeight)

	style := lipgloss.NewStyle().
		Border(lipgloss.RoundedBorder()).
		Width(m.viewSize.LeftPaneWidth).
		Height(m.viewSize.TabContentHeight)

	if m.currentBuffer == LeftPaneTabsNode {
		style = style.BorderForeground(lipgloss.Color("#00ff9c"))
	}

	return style.Render(m.environment.listView.View())
}

func (m State) RenderEnvironment() string {
	switch m.currentLeftPaneTab {
	case EnvironmentEditNode, EnvironmentNameEditNode:
		return m.RenderEnvironmentEdit()
	case EnvironmentNode:
		return m.RenderEnvironmentList()
	default:
		return m.RenderEnvironmentList()
	}
}
