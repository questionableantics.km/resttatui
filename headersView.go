package main

import (
	"github.com/charmbracelet/bubbles/textarea"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

func (m State) HandleHeadersViewUpdate(msg tea.Msg) (State, tea.Cmd) {
	var cmd tea.Cmd

	m.headersView, cmd = m.headersView.Update(msg)

	return m, cmd
}

func NewHeadersView(viewSize ViewSize) textarea.Model {
	headersView := textarea.New()
	headersView.SetHeight(viewSize.TabContentHeight + 2)
	headersView.SetWidth(viewSize.RightPaneWidth + 24)

	return headersView
}

// //////////////////////////////////////////////////////////////////////////
// Bubble Tea
// //////////////////////////////////////////////////////////////////////////

func (m State) RenderHeaders() string {
	m.headersView.SetWidth(m.viewSize.RightPaneWidth)
	m.headersView.SetHeight(m.viewSize.TabContentHeight)
	headersStyle := lipgloss.NewStyle().
		Border(lipgloss.RoundedBorder()).
		Width(m.viewSize.RightPaneWidth).
		Height(m.viewSize.TabContentHeight)

	if m.currentRightPaneTab == HeadersNode && m.currentBuffer.Name == RightPaneTabsNode.Name {
		return headersStyle.
			BorderForeground(lipgloss.Color("#00ff9c")).
			Render(m.headersView.View())
	}

	return headersStyle.Render(m.headersView.View())
}
