package main

import (
	"fmt"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

// //////////////////////////////////////////////////////////////////////////
// Consts
// //////////////////////////////////////////////////////////////////////////

var (
	ResponseNode = &StringNode{
		Name: "Response",
	}
	BodyNode = &StringNode{
		Name: "Body",
		OnBlur: func(m State) (State, tea.Cmd) {
			m.bodyView.Blur()

			return m, nil
		},
		OnFocus: func(m State) (State, tea.Cmd) {
			m.bodyView.Focus()

			return m, nil
		},
	}
	HeadersNode = &StringNode{
		Name: "Headers",
		OnBlur: func(m State) (State, tea.Cmd) {
			m.headersView.Blur()

			return m, nil
		},
		OnFocus: func(m State) (State, tea.Cmd) {
			m.headersView.Focus()

			return m, nil
		},
	}
	ParamsNode = &StringNode{
		Name: "Params",
		OnBlur: func(m State) (State, tea.Cmd) {
			m.paramsView.Blur()

			return m, nil
		},
		OnFocus: func(m State) (State, tea.Cmd) {
			m.paramsView.Focus()

			return m, nil
		},
	}
)

func initializeTabOrder() {
	ResponseNode.Next = BodyNode
	BodyNode.Next = HeadersNode
	HeadersNode.Next = ParamsNode
	ParamsNode.Next = ResponseNode

	ResponseNode.Prev = ParamsNode
	ParamsNode.Prev = HeadersNode
	HeadersNode.Prev = BodyNode
	BodyNode.Prev = ResponseNode
}

// slice representing the order of tabs that a user can tab through
var Tabs = []string{
	ResponseNode.Name,
	BodyNode.Name,
	HeadersNode.Name,
	ParamsNode.Name,
}

// //////////////////////////////////////////////////////////////////////////
// Bubble Tea
// //////////////////////////////////////////////////////////////////////////

func (m State) RenderTabs() string {
	var tabs []string

	style := lipgloss.NewStyle().
		BorderBottom(true).
		Width(m.viewSize.RightPaneWidth / len(Tabs)).
		Height(m.viewSize.TabHeaderHeight).
		Align(lipgloss.Center)

	focusedStyle := style.Copy().
		Foreground(lipgloss.Color("#00ff9c")).
		BorderBottom(true)

	for _, tab := range Tabs {
		v := fmt.Sprintf("%s", tab)

		if tab == m.currentRightPaneTab.Name {
			tabs = append(tabs, focusedStyle.Render(v))
		} else {
			tabs = append(tabs, style.Render(v))
		}
	}

	return lipgloss.JoinHorizontal(lipgloss.Right, tabs...)
}
