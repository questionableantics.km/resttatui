# Planned
- Open given input in scratch buffer of vim/neovim
    - Set buffer type to appropiate filetype
        - i.e. if editing body and body type is `json`, set buffer filetype to `json`
