package main

import (
	"fmt"

	tea "github.com/charmbracelet/bubbletea"
)

func GoTo(current *StringNode, target *StringNode, state State) (State, *StringNode, tea.Cmd) {
	var cmds []tea.Cmd
	var cmd tea.Cmd

	if current.OnBlur != nil {
		state, cmd = current.OnBlur(state)
		cmds = append(cmds, cmd)
	}

	current = target

	if current.OnFocus != nil {
		state, cmd = current.OnFocus(state)
		cmds = append(cmds, cmd)
	}

	return state, current, tea.Batch(cmds...)
}

func GoNext(current *StringNode, state State) (State, *StringNode, tea.Cmd) {
	return GoTo(current, current.Next, state)
}

func GoPrev(current *StringNode, state State) (State, *StringNode, tea.Cmd) {
	return GoTo(current, current.Prev, state)
}

func (m State) GoNextRightPaneTab() (State, tea.Cmd) {
	var cmd tea.Cmd

	m, m.currentRightPaneTab, cmd = GoNext(m.currentRightPaneTab, m)

	return m, cmd
}

func (m State) GoPrevRightPaneTab() (State, tea.Cmd) {
	var cmd tea.Cmd

	m, m.currentRightPaneTab, cmd = GoPrev(m.currentRightPaneTab, m)

	return m, cmd
}

func (m State) GoNextBuffer() (State, tea.Cmd) {
	var cmd tea.Cmd

	m, m.currentBuffer, cmd = GoNext(m.currentBuffer, m)

	return m, cmd
}

func (m State) GoToLeftPaneTab(target *StringNode) (State, tea.Cmd) {
	var cmd tea.Cmd

	m, m.currentLeftPaneTab, cmd = GoTo(m.currentLeftPaneTab, target, m)

	return m, cmd
}

func (m State) GoPrevBuffer() (State, tea.Cmd) {
	var cmd tea.Cmd

	m, m.currentBuffer, cmd = GoPrev(m.currentBuffer, m)

	return m, cmd
}

func p80(s interface{}) {
	fmt.Println("=========================================")
	fmt.Println("\n\n\n\n\n\n\n\n\n\n\n\n")
	fmt.Println(s)
	fmt.Println("\n\n\n\n\n\n\n\n\n\n\n\n")
	fmt.Println("=========================================")
}
