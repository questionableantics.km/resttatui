package main

import (
	"bytes"
	"encoding/json"
	"fmt"

	"github.com/TylerBrock/colorjson"
	"github.com/alecthomas/chroma/quick"
	"github.com/atotto/clipboard"
	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

func formatJson(jsonString []byte) string {
	var obj interface{}
	json.Unmarshal([]byte(jsonString), &obj)
	f := colorjson.NewFormatter()
	f.Indent = 2
	jsonString, err := f.Marshal(obj)

	if err != nil {
		return fmt.Sprintf("%s", err)
	}

	return string(jsonString)
}

func colorizeJson(jsonString string) string {
	var buf bytes.Buffer
	err := quick.Highlight(&buf, jsonString, "json", "terminal16m", "monokai")

	if err != nil {
		return fmt.Sprintf("%s", err)
	}

	return string(buf.Bytes())
}

func (m State) HandleResponseViewUpdate(msg tea.Msg) (State, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		key := msg.String()

		switch key {
		case "j", "down":
			m.responseView.LineDown(1)
		case "k", "up":
			m.responseView.LineUp(1)
		case "g":
			m.responseView.GotoTop()
		case "G":
			m.responseView.GotoBottom()
		case "u":
			m.responseView.HalfViewUp()
		case "d":
			m.responseView.HalfViewDown()
		case "y":
			// TODO: this probably has some funky encoding from colorjson
			clipboard.WriteAll(m.response)
		}
	}

	return m, nil
}

func NewResponseView(viewSize ViewSize) viewport.Model {
	return viewport.New(viewSize.RightPaneWidth, viewSize.TabContentHeight)
}

func (m State) RenderResponse() string {
	m.responseView.Height = m.viewSize.TabContentHeight
	m.responseView.Width = m.viewSize.RightPaneWidth

	style := lipgloss.
		NewStyle().
		Border(lipgloss.RoundedBorder()).
		Height(m.viewSize.TabContentHeight).
		Width(m.viewSize.RightPaneWidth)

	if m.currentBuffer.Name == RightPaneTabsNode.Name {
		return style.
			BorderForeground(lipgloss.Color("#00ff9c")).
			Render(m.responseView.View())
	}

	return style.Render(m.responseView.View())
}
